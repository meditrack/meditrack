// STL and std C++ headers
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

// Tesseract headers
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

// Opencv header
#include <opencv2/opencv.hpp>

#define DEBUG 1

// Class to hold the detected text rectangle and corresponding recognised text.
class RecognisedText {
public:
	std::string text;
	int x;
	int y;
	int width;
	int height;
public:
	RecognisedText(std::string text, int x, int y, int width, int height) {
		this->text = text;
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
	}
};

tesseract::TessBaseAPI* initTesseract() {
	tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
	api->SetOutputName("debug_log.txt");

	// Initialize tesseract-ocr with English, without specifying tessdata path
	if (api->Init(NULL, "eng")) {
		std::cerr << "Could not initialize tesseract." << std::endl;
		exit(1);
	}
	api->SetVariable("save_blob_choices", "T");
	return api;
}

std::vector<RecognisedText> recogniseText(tesseract::TessBaseAPI* api,
		cv::Mat& image, cv::Rect& rect) {

	api->SetImage(image.data, image.cols, image.rows,
			image.channels(), image.step);

	api->SetRectangle(rect.x, rect.y, rect.width, rect.height);
	api->Recognize(NULL);

	std::vector<RecognisedText> recognised_lines;
	tesseract::ResultIterator* ri = api->GetIterator();
	tesseract::PageIteratorLevel level = tesseract::RIL_TEXTLINE;
	if (ri != 0) {
		do {
			std::string line = ri->GetUTF8Text(level);
			float conf = ri->Confidence(level);
			int x1, y1, x2, y2;
			ri->BoundingBox(level, &x1, &y1, &x2, &y2);
			RecognisedText rt(line, x1, y1, x2 - x1, y2 - y1);
			recognised_lines.push_back(rt);
		} while ((ri->Next(level)));
	}
	api->End();
	return (recognised_lines);
}

int main(int argc, char** argv) {
	std::ofstream file;
	std::vector<RecognisedText> recognised_words;
	if (argc < 3) {
		std::cout << "Usage: " << argv[0] << " <input_image>"
				<< " <output_text_file>" << std::endl;
		return (-1);
	}

	file.open(argv[2], std::ios::out);

	std::string image_file = argv[1];
	cv::Mat image;
	image = cv::imread(image_file, cv::IMREAD_GRAYSCALE);
	cv::Rect boundingBox(cv::Point(0, 0), cv::Point(image.cols, image.rows));

	tesseract::TessBaseAPI* api = initTesseract();

	recognised_words = recogniseText(api, image, boundingBox);

	cv::Mat cimage;
	cv::cvtColor(image, cimage, cv::COLOR_GRAY2BGR);

	if (!recognised_words.empty()) {
		for (int i = 0; i < recognised_words.size(); i++) {
			file << recognised_words[i].text << " "
			/*<< recognised_words[i].x << " "
			 << recognised_words[i].y << " "
			 << recognised_words[i].width << " "
			 << recognised_words[i].height << " "*/
			<< std::endl;
			cv::Rect r(recognised_words[i].x, recognised_words[i].y,
					recognised_words[i].width, recognised_words[i].height);
			cv::rectangle(cimage, r, cv::Scalar(0, 255, 0));
		}
		cv::imwrite("result.jpg", cimage);
	}
	file.close();
	return (0);
}
