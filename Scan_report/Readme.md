Welcome to Tesseract.

To build, this small piece of code, you will need, 
- Leptonica image processing library.
    If you are using Ubuntu, just install libleptonica-dev from Ubuntu repositories using apt.
    If you are using windows or Mac, check out
    http://www.leptonica.com/source/README.html
    The source is here
    http://www.leptonica.com/download.html
- Tesseract OCR engine
    https://github.com/tesseract-ocr
    clone into tesseract and tessdata repositories.
    https://code.google.com/p/tesseract-ocr/wiki/ReadMe contains detailed instructions for how to build for different platforms.
    
Once you have the two things in place, follow the following steps.
create a folder to put all your built files into (ex: build)
from inside the build directory, 
(If you do not have cmake, install it using apt, brew in ubuntu and mac. For windows its a zip file download.)
$cmake .. 
$make

To run, 
$scan_image <input image file> <output_text_file>
