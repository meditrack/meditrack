// Tesseract headers
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

// Invoke Tesseract OCR using baseapis.
// Input : image_path and rectangle to locate the text.
std::string recogniseText(std::string image_path) {
    std::string outText;

    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
    api->SetOutputName("debug_log.txt");

    // Initialize tesseract-ocr with English, without specifying tessdata path
    if (api->Init(NULL, "eng")) {
        std::cerr << "Could not initialize tesseract."
                  << std::endl;
        exit(1);
    }

    // Tell tesseract which image to look at.
    // api->SetImage((const unsigned char*)img->data.u8,
    //               img->cols, img->rows, 1, img->cols);
    // TODO(jayasimha) :The above call somehow skews the data and results in
    // gibberish. Need to investigate why and use it, as it avoids creating
    // a temp file.

    // Open the file in leptonica and tell tesseract to look at this image.
    Pix *image = pixRead(image_path.c_str());
    api->SetImage(image);
    // Tell tesseract where the text is
    //api->SetRectangle(rect->x, rect->y, rect->width, rect->height);

    // Get OCR result and trim it.
    outText = api->GetUTF8Text();
    //outText.erase(remove_if(outText.begin(), outText.end(), ::isspace),
   //              outText.end());

    // Destroy used object and release memory
    api->End();
    return (outText);
}

int main(int argc, char** argv) {
    std::ofstream file;
    std::string recognised_words;    
    if (argc < 3) {
        std::cout << "Usage: ./swtdetect <input_image>"
                  << "<output_text_file>"
                  << std::endl;
        return (-1);
    }

    file.open(argv[2], std::ios::out);

    recognised_words = recogniseText(argv[1]);

    std::cout << recognised_words <<std::endl;
    file << recognised_words <<std::endl;
    file.close();
    return (0);
}
